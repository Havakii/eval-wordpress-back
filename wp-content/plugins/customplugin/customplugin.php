<?php
/*
Plugin Name: Annuaire plugin
Plugin URI: https://hb.com/
Description: Table annuaire
Author: Hava Bakrieva
Version: 1.0
Author URI: http://hb.com/
*/
?>
<?php
function annuaire_func() {
?>
    <table width='70%' border='1' style='border-collapse: collapse;'>
    <tr>
    <th>ID</th>
    <th>Nom de l'entreprise</th>
    <th>Localisation</th>
    <th>Prénom</th>
    <th>Nom</th>
    <th>Adresse mail</th>
    </tr>
    <?php
    global $wpdb;
    $entriesList = $wpdb->get_results("SELECT * FROM wp_annuaire");
        foreach($entriesList as $entry){
        $id = $entry->id;
        $name = $entry->nom_entreprise;
        $localisation = $entry->localisation_entreprise;
        $firstname = $entry->prenom_contact;
        $lastname = $entry->nom_contact;
        $mail = $entry->mail_contact;
        echo "<tr>
        <td>".$id."</td>
        <td>".$name."</td>
        <td>".$localisation."</td>
        <td>".$firstname."</td>
        <td>".$lastname."</td>
        <td>".$mail."</td>
        </tr>
        ";
    }
    ?>
    </table>
<?php
}
add_shortcode( 'annuaire', 'annuaire_func' );